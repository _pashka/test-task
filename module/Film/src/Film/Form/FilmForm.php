<?php

namespace Film\Form;

use Zend\Form\Form;

class FilmForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('film');

        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
            'options' => array(
                'label' => 'Name',
            ),
        ));
        $this->add(array(
            'name' => 'year',
            'type' => 'Number',
            'options' => array(
                'label' => 'Year',
            ),
            'attributes' => array(
                'min' => 1940,
                'max' => 2016,
                'class' => 'col-xs-4',
            ),
        ));
        $this->add(array(
            'name' => 'isActive',
            'type' => 'checkbox',
            'options' => array(
                'label' => 'Is available'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}