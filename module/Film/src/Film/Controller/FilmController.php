<?php

namespace Film\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Film\Model\Film;
use Film\Form\FilmForm;

class FilmController extends AbstractActionController
{
    protected $filmTable;

    public function indexAction()
    {
        return new ViewModel(array(
            'films' => $this->getFilmTable()->fetchAll(),
        ));
    }

    public function addAction()
    {
        $form = new FilmForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $film = new Film();
            $form->setInputFilter($film->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $film->exchangeArray($form->getData());
                $this->getFilmTable()->saveFilm($film);

                return $this->redirect()->toRoute('home');
            }
        }
        return array('form' => $form);
    }

    public function additionalAction()
    {
        $request = $this->getRequest();
        $n = $request->getPost('n');

        $result = $this->createRandArray($n);

        return array('arr' => $result);
    }

    private function createRandArray($n, $array = null)
    {
        if(is_null($array))
            $array = array();

        if($n <= 0)
            return $array;
        else{
            $array[] = rand();
            return $this->createRandArray($n-1,$array);
        }

    }

    public function getFilmTable()
    {
        if (!$this->filmTable) {
            $sm = $this->getServiceLocator();
            $this->filmTable = $sm->get('Film\Model\FilmTable');
        }
        return $this->filmTable;
    }
}