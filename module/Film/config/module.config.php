<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Film\Controller\Film' => 'Film\Controller\FilmController',
        ),
    ),

    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Film\Controller\Film',
                        'action'     => 'index',
                    ),
                ),
            ),

            'film' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/film[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Film\Controller\Film',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ .'/../view/layout/layout.phtml',
            'film/index/index'        => __DIR__ .'/../view/film/film/index.phtml',
            'error/404'               => __DIR__ .'/../view/error/404.phtml',
            'error/index'             => __DIR__ .'/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . "/../view",
        ),
    ),
);


